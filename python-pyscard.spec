%global _empty_manifest_terminate_build 0
Name:		python-pyscard
Version:	2.2.0
Release:	1
Summary:	A framework for building smart card aware applications in Python
License:	LGPLv2+
URL:		https://github.com/LudovicRousseau/pyscard
Source0:	https://github.com/LudovicRousseau/pyscard/archive/refs/tags/pyscard-%{version}.tar.gz

%description
The pyscard smartcard library is a framework for building smart card aware
applications in Python. The smartcard module is built on top of the PCSC API
Python wrapper module.

%package -n python3-pyscard
Summary:	A framework for building smart card aware applications in Python
Provides:	python-pyscard = %{version}-%{release}
BuildRequires:  gcc
BuildRequires:  pcsc-lite-devel
BuildRequires:  swig >= 1.3.31
BuildRequires:  python3-setuptools
BuildRequires:  python3-devel

%description -n python3-pyscard
The pyscard smartcard library is a framework for building smart card aware
applications in Python. The smartcard module is built on top of the PCSC API
Python wrapper module.

%prep
%autosetup -n pyscard-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pyscard -f filelist.lst
%license LICENSE
%doc ACKS README.md
%doc src/smartcard/doc/*
%dir %{python3_sitearch}/*

%changelog
* Mon Dec 23 2024 sqfu <dev01203@linx-info.com> - 2.2.0-1
- update to 2.2.0
- Fix typos found by typos applications
- Remove Python 2 conditional code
- Migrate to a src/ layout
- Support only Python 3.9 and higher
- Migrate CI to use the official Coveralls action
- Standardize local and CI testing to use tox
- Build the docs as a part of the test suite
- Remove the Python 2.x-only Pyro dependency

* Tue Oct 29 2024 muxiaohui <muxiaohui@kylinos.cn> - 2.1.0-1
- Update version to 2.1.0
- Fix deprecation warnings.
- Fix use of undefined variable 'hresult' in exceptions

* Tue Sep 3 2024 lilu <lilu@kylinos.cn> - 2.0.10-1
- Update package to version 2.0.10
- Add 'swig' in pyproject.toml build requires
- Fix a bug with CardRequestTimeoutException introduced in 2.0.8

* Tue Aug 06 2024 muxiaohui <muxiaohui@kylinos.cn> - 2.0.8-1
- Update package to version 2.0.8
- Make CardMonitor() thread safe on Python 3.12
- Add hresult value in exceptions
- Check swig is installed on build

* Tue Dec 26 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.0.7-2
- Fixed the problem that yubikey-manager cannot depend on pyscard

* Thu Aug 03 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 2.0.7-1
- package init
